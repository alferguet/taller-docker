# Taller Docker

## Asociación GNU/Linux Valencia

### Índice

1. Introducción a Docker
2. Instalación de Docker y Compose
3. Creación e interacción con una imagen
    1. Construcción de la imagen
    2. Ejecución de un contenedor
    3. Enlace de puertos y almacenamiento
4. Orquestación con Docker Compose

### 1. Introducción a Docker (~15 minutos)

Se plantea dar una breve introducción a Docker explicando
sus componentes y utilidades a partir de varios casos de uso.

### 2. Instalación de Docker y Compose (~15 minutos)

Se proporcionaran una serie de scripts que permitan instalar a los asistentes
Docker y Docker Compose en multiples distribuciones.

### 3. Creación e interacción con una imagen (~1 hora 30 minutos)

#### 3.1 Construcción de la imagen

Para contruir una imagen se presentara la sintaxis básica de Dockerfile,
construyendo de forma guiada una imagen que permita ejecutar un "Hola mundo" en
un contenedor.

#### 3.2  Ejecución de un contenedor

Tras la construcción de la imagen se explicara la forma de ejecutarla en un contenedor
con las opciones básicas y la posibilidad de lanzarla en segundo plano.

Se comprobará la correcta ejecución visualizando los registros de los contenedores.

#### 3.3 Enlace de puertos y almacenamiento

Una de las opciones más interesantes es poder enlazar puertos
para poder permitir la conectividad con las aplicaciones en
ejecución dentro de contenedores y disponer de almacenamiento
para poder conservar datos externamente.

Con la ejecución de una imagen de una base de datos, se emplearan las
opciones para permitir y comprobar como enlaza el puerto necesario
y almacena sus datos de forma externa.

### 4. Orquestación con Docker Compose (~1 hora)

Explicacion inicial de la orquestación mas un ejercicio practico.

A partir de una plantilla proporcionada en el repositorio se espera
que el usuario la rellene con la información de la imagen de la base de datos
utilizada anteriormente y otra proporcionada para guardar un registro en
la base de datos, estableciendo dependencias y una ejecución simultanea.
