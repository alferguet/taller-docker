Introducción a Docker
Asociación GNU/Linux Valencia

Alejandro Fernandez Huguet
Graduado en Ingenieria Informatica por la UJI
Trabajando en desarrollo de aplicaciones web

¿Qué es Docker?

Virtualización de contenedores
Ejecucción de utilidades o servicios
Creacion de entornos aislados y conflictivos

¿Qué compone docker?

Daemon
Contenedores
CLI

¿Qué es un contenedor?

@pics/onion.jpg

Virtualización del software
Recursos de la maquina compartidos
Ligereza por su arquitectura

Uso de Docker

Despliegue de servicios aislados
Resolución de conflictos sobre dependencias
Gestión sencilla a traves de su CLI

Casos de uso

Despliegue infraestructura aplicación web
Microservicios independientes
Aplicaciones legacy o incompatibles
Ejecucción de utilidades sobre un sistema limpio
Entornos de pruebas/desarrollo

Instalación de Docker

Scripts automaticos disponibles en la carpeta "instalacion"

Construccion de un "Hola Mundo" en Docker

Creación del contenedor mediante Dockerfile

FROM

WORKDIR

COPY/ADD

EXPOSE

CMD

Uso de Docker CLI

docker build -t <imagen>:<tag> .

Etiqueta SIEMPRE tus contenedores

Ejecucción del contenedor

docker run -it -name <nombre contenedor> -p 3000:3000 <nombre imagen>
<comando opcional> <parametros opcionales>

Comprobamos su ejecución

docker ps

Acceder via navegador 
localhost:3000

Acceder al registro del contenedor

docker logs <nombre contenedor>
docker logs -f <nombre contenedor>

Paramos el contenedor

docker stop <nombre contenedor>

Comprobamos estado

docker ps -a

Enlace de puertos

docker run -p <puerto externo>:<puerto interno>
docker run --network host 

Enlace de almacenamiento

docker run -v <nombre volumen>:<ubicacion interna>
docker run -v <ubicacion externa>:<ubicacion interna>

Orquestación de contenedores

Gestionar contenedores en lote
Establecimiento de dependencias y reglas
Configuracion de todos los contenedores en un mismo fichero

Construcción del fichero mediante YAML

docker-compose up -d 

docker-compose down
